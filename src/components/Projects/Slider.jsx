import React, { useRef } from 'react'
import Slider from 'react-slick';
import Project from './Project';
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import styled from 'styled-components';

let data = [
    {
      lien: "https://www.admin.leconseillerdubtp.com/fr/connexion",
      desc_name : "Web BTP advisor",
      img : "/cbtp_web.png",
      disc : "CBTP web is a web project built with React Native. It's an application that interconnects players in the construction sector with each other and with customers."
    },
    {
      lien: "https://app.inawo.pro/fr/connexion",
      desc_name : "App Inawo",
      img : "/inawo_app.png",
      disc : "Inawo App is a management and billing software suite based in Africa. It distributes Internet-based management software."
    },
    {
      lien: "",
      desc_name : "Mobile BTP advisor",
      img : "/cbtp_logo2.png",
      disc : "CBTP mobile is a mobile project built with React Native. It's an application that interconnects players in the construction sector with each other and with customers."
    },
    {
      lien: "https://fb-handball.com/",
      desc_name : "Benin Handball",
      img : "/fede_site.png",
      disc : "The FBHB website offers a variety of information about handball in Benin, including news, competition information, magazines, a weekly newsletter, etc..."
    },
    {
      lien: "https://inawo.pro/",
      desc_name : "Inawo",
      img : "/inawo_site.png",
      disc : "Inawo Technologies is a software publisher based in Africa. It distributes Internet-based management software."
    }
];

var settings = {
    className: "center",
    centerMode: true,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows : false,
    responsive: [
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
          centerMode : false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
          centerMode : false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode : false
        }
      }
    ]
  };
const SliderComp = () => {
  const arrowRef = useRef(null);
    let sliderProject = "";
    sliderProject = data.map((item, i) => (
        <Project item = {item} key={i}/>
    ))
  return (
    <Container>
      <Slider ref={arrowRef} {...settings}>
      {sliderProject}
      </Slider>
      <Buttons>
        <button 
        onClick={() => arrowRef.current.slickPrev()}
        className='back'><IoIosArrowBack/></button>
        <button 
        onClick={() => arrowRef.current.slickNext()}
        className='next'><IoIosArrowForward/></button>
      </Buttons>
    </Container>
  )
}

export default SliderComp;

const Container = styled.div`
  position: relative;
`

const Buttons = styled.div`
  button{
    width: 2rem;
    height: 2rem;
    background-color: rgba(255, 255, 255, 0.100);
    cursor: pointer;
    color: #318CE7;
    border: none;
    position: absolute;
    top: 45%;
    right: -1rem;
  }

  .back{
    left: -1rem;
  }
`