import React from "react";
import styled from "styled-components";
import { AiOutlineInstagram } from "react-icons/ai";
import { GiEarthAmerica } from "react-icons/gi";
import { FaLinkedinIn } from "react-icons/fa";
import { Slide } from "react-awesome-reveal";
// import document from '../../../public/static/CV-LEO-LIMOAN.pdf'

const ProfComponent = () => {
  return (
    <Container id="home">
      <Slide direction="left">
        <Texts>
          <h4>
            Hello <span className="bleu">I'am</span>
          </h4>
          <h1 className="bleu">Leo Lazare N. LIMOAN</h1>
          <h3>Web & Mobile Developer</h3>
          <p>
            With a Bachelor's degree in Computer Science and a passion for
            code and technological strategies, I've been working in this
            field for the past few years to perfect my knowledge and skills.
            Curious, rigorous and passionate about passing on my knowledge,
            it's thanks to my professional experience that I've been able to
            hone my approach to the field as CTO and lead ReactJs developer
            for a team. Not only that, I also have skills in backend
            (PHP and Python), frontend (ReactJs and VueJs) and mobile (React Native).
          </p>
          <a href="/static/CV-LEO-LIMOAN.pdf" download=""  style={{marginRight: "20px"}}><button>Download CV</button></a>
          <a href="#footer"><button>Let's talk</button></a>
          {/* <div className="" style={{opacity: 1, transform: "none"}}>
          <a href="/static/media/cv.7bca10997366c832c08c.pdf" download="" class="btn">Download CV</a>
          <a href="#contact" className="btn btn-primary">Let's Talk</a></div> */}
          <Social>
            <p>Check out my</p>
            <div className="social-icons">
              <span>
                <a href="/">
                  <AiOutlineInstagram />
                </a>
              </span>
              <span>
                <a href="https://gitlab.com/levrai">
                  <GiEarthAmerica />
                </a>
              </span>
              <span>
                <a href="https://www.linkedin.com/in/leo-limoan-a15ba323b">
                  <FaLinkedinIn />
                </a>
              </span>
            </div>
          </Social>
        </Texts>
      </Slide>
      <Slide direction="right">
        <Profile>
          <img
            style={{ borderRadius: "50%" }}
            src={process.env.PUBLIC_URL + "/mon_image.JPG"}
            alt="profile"
          />
        </Profile>
      </Slide>
    </Container>
  );
};

export default ProfComponent;

const Container = styled.div`
  display: flex;
  gap: 2rem;
  padding-top: 3rem;
  width: 80%;
  max-width: 1280px;
  margin: 0 auto;
  z-index: 1;
  @media (max-width: 840px) {
    width: 90%;
  }

  @media (max-width: 640px) {
    flex-direction: column;
  }
`;
const Texts = styled.div`
  flex: 1;
  h4 {
    padding: 1rem 0;
    font-weight: 500;
  }
  h1 {
    font-size: 2rem;
    font-family: "Secular One", sans-serif;
    letter-spacing: 2px;
  }
  h3 {
    font-weight: 500;
    font-size: 1.2rem;
    padding-bottom: 1.2rem;
    text-transform: capitalize;
  }
  p {
    font-weight: 300;
  }

  button {
    padding: 0.7rem 2rem;
    margin-top: 3rem;
    cursor: pointer;
    background-color: #318CE7;
    border: none;
    color: #fff;
    font-weight: 500;
    filter: drop-shadow(0px 10px 10px #318CE751);
    :hover {
      filter: drop-shadow(0px 10px 10px #318CE770);
    }
  }
`;
const Social = styled.div`
  margin-top: 3rem;
  display: flex;
  align-items: center;
  gap: 1rem;
  p {
    font-size: 0.9rem;
    @media (max-width: 690px) {
      font-size: 0.7rem;
    }
  }

  .social-icons {
    display: flex;
    align-items: center;
    gap: 1rem;
    span {
      width: 2.3rem;
      height: 2rem;
      clip-path: polygon(25% 0%, 75% 0%, 100% 50%, 75% 100%, 25% 100%, 0% 50%);
      background-color: #318CE7;
      position: relative;
      transition: transform 400ms ease-in-out;
      :hover {
        transform: rotate(360deg);
      }
    }

    a {
      color: #fff;
      position: absolute;
      top: 55%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
  }
`;
const Profile = styled.div`
  img {
    width: 25rem;
    filter: drop-shadow(0px 10px 10px #318CE770);
    transition: transform 400ms ease-in-out;
    @media (max-width: 790px) {
      width: 20rem;
    }

    @media (max-width: 660px) {
      width: 18rem;
    }

    @media (max-width: 640px) {
      width: 100%;
    }
  }

  :hover img {
    transform: translateY(-10px);
  }
`;
